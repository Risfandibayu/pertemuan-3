package bayu.risfandi.pertemuan3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "students"
    val F_ID = "id"
    val F_NAME = "name"
    val F_ADDRESS = "address"
    val F_PHONE = "phone"
    var docId = ""

    lateinit var db : FirebaseFirestore
    lateinit var alStudent : ArrayList<HashMap<String, Any>>
    lateinit var adapter : SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alStudent = ArrayList()
        button_simpan.setOnClickListener(this)
        button_delete.setOnClickListener(this)
        button_simpan.setOnClickListener(this)
        button_update.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for(doc in result){
                val hm = HashMap<String, Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME,doc.get(F_NAME).toString())
                hm.set(F_ADDRESS,doc.get(F_ADDRESS).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(this,alStudent,R.layout.row_data,
                arrayOf(F_ID,F_NAME,F_ADDRESS,F_PHONE),
                intArrayOf(R.id.textView_ID, R.id.textView_Name, R.id.textView_Address, R.id.textView_phone))
            lsData.adapter = adapter
        }
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e != null) Log.d("fireStore",e.message.toString())
            showData()
        }
    }
    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val hm = alStudent.get(position)
        docId = hm.get(F_ID).toString()
        edID.setText(docId)
        edName.setText(hm.get(F_NAME).toString())
        edAddress.setText(hm.get(F_ADDRESS).toString())
        editPhone.setText(hm.get(F_PHONE).toString())

    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.button_simpan -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, edID.text.toString())
                hm.set(F_NAME, edName.text.toString())
                hm.set(F_ADDRESS, edAddress.text.toString())
                hm.set(F_PHONE, editPhone.text.toString())
                db.collection(COLLECTION).document(edID.text.toString()).set(hm).addOnSuccessListener {
                    Toast.makeText(this, "Data successfull added", Toast.LENGTH_LONG).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data failed to added ${e.message}", Toast.LENGTH_LONG).show()
                }
            }
            R.id.button_update -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, edID.text.toString())
                hm.set(F_NAME, edName.text.toString())
                hm.set(F_ADDRESS, edAddress.text.toString())
                hm.set(F_PHONE, editPhone.text.toString())
                db.collection(COLLECTION).document(edID.text.toString()).update(hm).addOnSuccessListener {
                    Toast.makeText(this, "Data successfull updated", Toast.LENGTH_LONG).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data failed to updated ${e.message}", Toast.LENGTH_LONG).show()
                }
            }

            R.id.button_delete -> {
                db.collection(COLLECTION).whereEqualTo(F_ID, docId).get()
                    .addOnSuccessListener { results ->
                        for (doc in results) {
                            db.collection(COLLECTION).document(doc.id).delete()
                                .addOnSuccessListener {
                                    Toast.makeText(
                                        this,
                                        "Data successfully deleted",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                                .addOnFailureListener {
                                    Toast.makeText(this, "Data failed deleted", Toast.LENGTH_SHORT)
                                        .show()
                                }
                        }

                    }.addOnFailureListener { e ->
                        Toast.makeText(this, "Can't get data references", Toast.LENGTH_SHORT).show()
                    }
            }

        }
    }


}